const crypto = require("crypto");
const jwt = require("jsonwebtoken");

const DADOS_CRIPTOGRAFAR = {
  algoritmo: "aes256",
  segredo: "chaves",
  tipo: "hex",
};

const formatPhone = (phone) => {
  formatNumber =
    phone.length === 13
      ? phone.match(/(\d{2})(\d{2})(\d{5})(\d{4})/)
      : phone.match(/(\d{2})(\d{2})(\d{4})(\d{4})/);
  var finalNumber =
    "+" +
    formatNumber[1] +
    " (" +
    formatNumber[2] +
    ") " +
    formatNumber[3] +
    "-" +
    formatNumber[4];
  return finalNumber;
};

const criptograph = (passw) => {
  const mykey = crypto.createCipher("aes-128-cbc", passw);
  let mystr = mykey.update("abc", "utf8", "hex");
  mystr += mykey.final("hex");

  return mystr;
};

const deCriptograph = (passw) => {
  const decipher = crypto.createDecipher("aes-128-cbc", passw);
  let mystr = decipher.update(passw, "utf8", "hex");
  mystr += decipher.final();
  return mystr;
};

//const verifyJWT = (req, res, next) {
const verifyJWT = (req, res, next) => {
  const token = req.headers["authorization"];

  if (!token)
    return res.status(401).json({ auth: false, message: "Não autenticado." });

  jwt.verify(token, process.env.SECRET, (err, userId) => {
    if (err) return res.sendStatus(403);
    req.user = userId;
    next();
  });
};

const httpeReturn = (res, code, success, msg, data = []) => {
  return res.status(code).json({
    sucess: success,
    msg: msg,
    data: data,
  });
};

module.exports = {
  formatPhone,
  criptograph,
  deCriptograph,
  verifyJWT,
  httpeReturn,
};
