const express = require("express");
const routes = express.Router();
const { verifyJWT } = require("../Helpers");

const UserController = require("../controllers/UserController");

routes.get("/users", verifyJWT, (req, res) => UserController.getAll(req, res));
routes.post("/users", verifyJWT, (req, res) => UserController.create(req, res));
routes.post("/auth", UserController.auth);

module.exports = routes;
