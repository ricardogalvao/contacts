const express = require("express");
const routes = express.Router();
const { verifyJWT } = require("../Helpers");

const ContactsController = require("../controllers/ContactsController");

routes.get("/contacts", verifyJWT, (req, res) =>
  ContactsController.getAll(req, res)
);
routes.post("/contacts", verifyJWT, (req, res) =>
  ContactsController.createContacts(req, res)
);

module.exports = routes;
