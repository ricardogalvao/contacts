const express = require("express");
const routes = express.Router();

const contacts = require("./contactsRoutes");
const users = require("./usersRoutes");

module.exports = (app) => {
  app.use(contacts, users);
};
