const yup = require("yup");
const { createMany, findAll } = require("../services/contacts");
const { httpeReturn } = require("../Helpers");

const contactSchema = yup.array().of(
  yup.object().shape({
    name: yup.string().required("O nome é obrigatório."),
    cellphone: yup
      .string()
      .min(12, "Telefone deve ter no mínimo 14 caracteres")
      .max(13, "Telefone deve ter no mínimo 14 caracteres")
      .required("Campo cellphone é obrigatório"),
  })
);

class ContactController {
  static async getAll(req, res) {
    const { filter } = req.query;
    let response;
    try {
      return res.status(200).json(await findAll());
    } catch (error) {
      return res.status(500).json(error);
    }
  }

  static async createContacts(req, res) {
    try {
      const contactList = req.body.contacts;
      await contactSchema.validate(contactList).catch((validation) => {
        return httpeReturn(res, 403, false, validation.message);
      });
      return res.status(200).json({
        sucess: true,
        msg: "Registros inseridos com sucesso",
        data: await createMany(contactList),
      });
    } catch (error) {
      console.log(error);
      return res.status(500).json(error);
    }
  }
}

module.exports = ContactController;
