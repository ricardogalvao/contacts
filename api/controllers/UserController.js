const yup = require("yup");
const { create, findAll, auth, validateUser } = require("../services/users");
const { httpeReturn } = require("../Helpers");
const userSchema = yup.object().shape({
  name: yup.string().required("O nome é obrigatório."),
  email: yup.string().required("O email é obrigatório."),
  password: yup
    .string()
    .min(5, "Senha deve ter no mínimo 6 caracteres")
    .required("A senha é obrigatória"),
});

class UsersController {
  static async getAll(req, res) {
    console.log(req, res);
    const { filter } = req.query;
    try {
      return res.status(200).json(await findAll());
    } catch (error) {
      return res.status(500).json(error);
    }
  }

  static async create(req, res) {
    try {
      const user = req.body;
      await userSchema.validate(user).catch((validation) => {
        return httpeReturn(res, 403, false, validation.message);
      });
      if (!(await validateUser(user))) {
        return httpeReturn(res, 406, false, "Usuário já cadastrado");
      }
      const newUser = await create(user);
      return httpeReturn(
        res,
        201,
        true,
        "Usuário cadastrado com sucesso",
        user
      );
    } catch (error) {
      return httpeReturn(res, 500, false, error);
    }
  }

  static async auth(req, res) {
    try {
      const user = req.body;
      const authentication = await auth(user);
      return httpeReturn(
        res,
        200,
        true,
        "login realizado com sucesso!",
        authentication
      );
    } catch (error) {
      return httpeReturn(res, 500, false, error);
    }
  }

  static async logout(req, res) {
    try {
      return httpeReturn(res, 200, true, { auth: false, token: null });
    } catch (error) {
      return httpeReturn(res, 500, false, error);
    }
  }
}

module.exports = UsersController;
