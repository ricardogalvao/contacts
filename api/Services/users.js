const database = require("../models");
const { criptograph } = require("../Helpers");
require("dotenv-safe").config();
const jwt = require("jsonwebtoken");

const validateUser = async (objUser) => {
  const validate = await database.Users.findOne({
    where: { email: objUser.email },
  });

  return validate ? false : true;
};

const create = async (record) => {
  record.password = criptograph(record.password);
  return await database.Users.create(record);
};

const findAll = async () => {
  return await database.Users.findAll();
};

const auth = async (objUser) => {
  const user = await database.Users.findOne({
    where: { email: objUser.email, password: criptograph(objUser.password) },
  });

  if (user) {
    const userId = user.dataValues.id;
    const token = jwt.sign({ userId }, process.env.SECRET, {
      expiresIn: 30000,
    });

    return {
      token: token,
    };
  }
};

const logout = async () => {};

module.exports = { create, findAll, auth, validateUser };
