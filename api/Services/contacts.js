const database = require("../models");
const { formatPhone, formatName } = require("../Helpers");

const createMany = async (records) => {
  const result = Promise.all(
    records.map(async (record) => {
      record.name = record.name.toUpperCase();
      record.cellphone = formatPhone(record.cellphone);
      return await database.Contacts.create(record);
    })
  );

  return result;
};

const findAll = async () => {
  return await database.Contacts.findAll();
};

module.exports = { createMany, findAll };
