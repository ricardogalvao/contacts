"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    queryInterface.addIndex("Contacts", ["cellphone"], {
      indexName: "EmailUniqueIndex",
      indicesType: "UNIQUE",
    });
  },

  down: async (queryInterface, Sequelize) => {
    queryInterface.removeIndex("Contacts", "EmailUniqueIndex");
  },
};
